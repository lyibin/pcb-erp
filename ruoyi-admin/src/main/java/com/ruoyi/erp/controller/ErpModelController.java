package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpCustomer;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpModel;
import com.ruoyi.erp.service.IErpModelService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产型号Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpModel")
public class ErpModelController extends BaseController
{
    private String prefix = "erp/erpModel";

    @Autowired
    private IErpModelService erpModelService;

    @RequiresPermissions("erp:erpModel:view")
    @GetMapping()
    public String erpModel()
    {
        return prefix + "/erpModel";
    }

    /**
     * 查询生产型号列表
     */
    @RequiresPermissions("erp:erpModel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpModel erpModel)
    {
        startPage();
        List<ErpModel> list = erpModelService.selectErpModelList(erpModel);
        return getDataTable(list);
    }

    /**
     * 导出生产型号列表
     */
    @RequiresPermissions("erp:erpModel:export")
    @Log(title = "生产型号", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpModel erpModel)
    {
        List<ErpModel> list = erpModelService.selectErpModelList(erpModel);
        ExcelUtil<ErpModel> util = new ExcelUtil<ErpModel>(ErpModel.class);
        return util.exportExcel(list, "erpModel");
    }

    /**
     * 新增生产型号
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存生产型号
     */
    @RequiresPermissions("erp:erpModel:add")
    @Log(title = "生产型号", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpModel erpModel)
    {
        return toAjax(erpModelService.insertErpModel(erpModel));
    }

    /**
     * 修改生产型号
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpModel erpModel = erpModelService.selectErpModelById(id);
        mmap.put("erpModel", erpModel);
        return prefix + "/edit";
    }

    /**
     * 修改保存生产型号
     */
    @RequiresPermissions("erp:erpModel:edit")
    @Log(title = "生产型号", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpModel erpModel)
    {
        return toAjax(erpModelService.updateErpModel(erpModel));
    }

    /**
     * 删除生产型号
     */
    @RequiresPermissions("erp:erpModel:remove")
    @Log(title = "生产型号", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpModelService.deleteErpModelByIds(ids));
    }

    @RequiresPermissions("erp:erpModel:view")
    @GetMapping("/pageSelectModel")
    public String pageSelectModel() {
        return prefix + "/pageSelectModel";
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpModel erpModel = erpModelService.selectErpModelById(id);
        mmap.put("erpModel", erpModel);
        return prefix + "/detail";
    }
}
